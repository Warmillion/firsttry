def check_if(a, b):
    # Повертаємо значення в залежності від виконання умов:
    return str(
        a + b
        if (a < b)
        else a - b
        if (a > b)
        else "game over"
        if (a == 0 and b == 0)
        else 0
    )


if __name__ == "__main__":
    # Вводимо значення для перевірки виконання умови
    x = int(input("Введіть ваш х: "))
    y = int(input("Введіть ваш y: "))

    # Виконуємо виклик функції:Передаємо задані значення та виводимо результат
    print(check_if(x, y))
